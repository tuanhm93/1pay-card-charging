const crypto = require('crypto');
const rp = require('request-promise');

const PAY = {
	access_key: '',
	secret:  '',
	timeout: 40000,
	return_url: ""
}

const setConfig = (options) => {
	PAY.access_key = options.access_key;
	PAY.secret = options.secret;
	PAY.return_url = options.return_url;
	options.timeout && (PAY.timeout = options.timeout);
}

// For begin transaction
const generateBeginSignature = (amount, command, order_id, order_info, return_url) => {
	let key = `access_key=${PAY.access_key}&amount=${amount}&command=${command}&order_id=${order_id}&order_info=${order_info}&return_url=${return_url}`;
	const signature = crypto.createHmac('sha256', PAY.secret).update(key).digest("hex");
	return signature;
}

const generateBeginURI = (amount, command, order_id, order_info, return_url, signature) => {
		return `https://api.pay.truemoney.com.vn/bank-charging/service/v2?access_key=${PAY.access_key}&amount=${amount}&command=${command}&order_id=${order_id}&order_info=${order_info}&return_url=${return_url}&signature=${signature}`;
}

const beginTransaction = (amount, order_id, order_info, cb) => {
  if(!PAY.access_key || !PAY.secret || !PAY.return_url) {
    throw new Error(`Not config secret, access_key for bank charging`);
  }

  const command = "request_transaction";
  const return_url = PAY.return_url;
  const signature = generateBeginSignature(amount, command, order_id, order_info, return_url);
  const URI = generateBeginURI(amount, command, order_id, order_info, return_url, signature);

  makeRequest(URI, cb)
}

// For end transaction
const generateEndSignature = (command, transRef) => {
  let key = `access_key=${PAY.access_key}&command=${command}&trans_ref=${transRef}`;
	const signature = crypto.createHmac('sha256', PAY.secret).update(key).digest("hex");
	return signature;
}

const generateEndURI = (command, transRef, signature) => {
  return `https://api.pay.truemoney.com.vn/bank-charging/service/v2?access_key=${PAY.access_key}&command=${command}&trans_ref=${transRef}&signature=${signature}`
}

const endTransaction = (transRef, cb) => {
  if(!PAY.access_key || !PAY.secret) {
    throw new Error(`Not config secret, access_key for bank charging`);
  }

  const command = "close_transaction";
  const signature = generateEndSignature(command, transRef);
  const URI = generateEndURI(command, transRef, signature);

  makeRequest(URI, cb);
}

// For check secure response
const generateSecureSigature = (info) => {
  const key = `access_key=${PAY.access_key}&amount=${info.amount}&card_name=${info.card_name}&card_type=${info.card_type}&order_id=${info.order_id}&order_info=${info.order_info}&order_type=${info.order_type}&request_time=${info.request_time}&response_code=${info.response_code}&response_message=${info.response_message}&response_time=${info.response_time}&trans_ref=${info.trans_ref}&trans_status=${info.trans_status}`
  return crypto.createHmac('sha256', PAY.secret).update(key).digest("hex");
}

const checkSecureResponse = (info) => {
  if(!PAY.access_key || !PAY.secret) {
    throw new Error(`Not config secret, access_key for bank charging`);
  }

  if(info.access_key !== PAY.access_key) {
    return false;
  }

  if(info.signature !== generateSecureSigature(info)) {
    return false;
  }

  return true;
}

// Recheck charging
const generateReCheckSignature = (trans_ref) => {
	let key = `access_key=${PAY.access_key}&command=get_transaction_detail&trans_ref=${trans_ref}`;
	const signature = crypto.createHmac('sha256', PAY.secret).update(key).digest("hex");
	return signature;
}

const generateReCheckURI = (command, transRef) => {
	return `https://api.pay.truemoney.com.vn/bank-charging/service/v2?access_key=${PAY.access_key}&command=${command}&trans_ref=${transRef}&signature=${signature}`;
}

const reCheck = (info, cb) => {
	if(!PAY.access_key || !PAY.secret) {
    throw new Error(`Not config secret, access_key for bank charging`);
  }
	const transRef = info.transRef;

	const command = "get_transaction_detail";
  const signature = generateReCheckSignature(transRef);
  const URI = generateEndURI(command, transRef, signature);

  makeRequest(URI, cb);
}


const makeRequest = (URI, cb) => {
	const options = {
		method: 'POST',
		uri: URI,
		timeout: PAY.timeout,
		json: true
	}

	rp(options)
	    .then((res) => {
	    	cb(null, res)
	    })
	    .catch((err) => {
    		cb(err);
	    });
}

module.exports = {
  config: setConfig,
  beginTransaction,
  endTransaction,
  checkSecureResponse,
	reCheck
}
