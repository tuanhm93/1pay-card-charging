const crypto = require('crypto');
const uuid = require('uuid/v4')
const rp = require('request-promise');

const config = {
	access_key: null,
	secret:  null,
	timeout: 3000,
	version: 5
}

const listDis = ['viettel', 'mobifone', 'vinaphone'];

const generateSignature = (type, pin, serial, transRef, reCheck = false, transId = '') => {
	let key = '';
	if(!reCheck) {
		key = `access_key=${config.access_key}&pin=${pin}&serial=${serial}&transRef=${transRef}&type=${type}`
	} else {
		key = `access_key=${config.access_key}&pin=${pin}&serial=${serial}&transId=$transId}&transRef=${transRef}&type=${type}`
	}
	 
	const signature = crypto.createHmac('sha256', config.secret).update(key).digest("hex");
	return signature;
}

const generateURI = (type, pin, serial, transRef, signature, reCheck = false, transId = '') => {
	if(reCheck) {
		return `https://api.1pay.vn/card-charging/v${config.version}/query?access_key=${config.access_key}&pin=${pin}&serial=${serial}&transRef=${transRef}&type=${type}&signature=${signature}&transId=${transId}`;
	}
	return `https://api.1pay.vn/card-charging/v${config.version}/topup?access_key=${config.access_key}&pin=${pin}&serial=${serial}&transRef=${transRef}&type=${type}&signature=${signature}`;
}

const setConfig = (options) => {
	config.access_key = options.access_key;
	config.secret = options.secret;
	options.timeout && (config.timeout = options.timeout);
	options.version && (config.version = options.version);
}

const sendPost = (type, pin, serial, cb) => {
	if(!config.access_key || !config.secret) {
		return cb(new Error('Not config access_key, secret'));
	}

	if( (listDis.indexOf(type) === -1) || !pin || !serial) {
		return cb(new TypeError(`type or pin or serial is not valid`));
	}

	const transRef = uuid();
	const signature = generateSignature(type, pin, serial, transRef);
	const URI = generateURI(type, pin, serial, transRef, signature);

	// Send post to gateway
	makeRequest(URI, cb);
}

const reCheckCard = (type, pin, serial, transRef, transId, cb) => {
	const signature = generateSignature(type, pin, serial, transRef, true, transId);
	const URI = generateURI(type, pin, serial, transRef, signature, true, transId);
	// Send post to gateway
	makeRequest(URI, cb);
}

const makeRequest = (URI, cb) => {
	const options = {
		method: 'POST',
		uri: URI,
		timeout: config.timeout,
		json: true
	}

	rp(options)
	    .then((res) => {
	    	cb(null, res)
	    })
	    .catch((err) => {
    		cb(err);
	    });
}

module.exports = {
	config: setConfig,
	cardCharging: sendPost
}